﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            int preferedSize;
            Console.Write("Podaj rozmiar planszy: ");
            preferedSize = int.Parse(Console.ReadLine()); 
            Program p = new Program(preferedSize);
            p.game();
        }

        /** ZMIENNE
         * sizeofGame - zmienna przechowujaca rozmiar planszy
         * deadCell - znak odpowiadajacy martwej komorce
         * aliveCell - znak odpowiadajacy zywej komorce
         * board - tablica przechowujaca znaki, odpowiada za plansze
         * newBoard - nowa tablica, do ktorej wpisywane sa komorki po danje turze
         */
        public int sizeOfGame;
        public const char deadCell = 'X';
        public const char aliveCell = 'O';

        public char[,] board; 
        public char[,] newBoard;

        /** KONSTRUKTOR
         * Ma za zadanie inicjalizować plansze kwadratowa o rozmiarze
         * wskazanym przez gracza. Wypełnia ją losowo za pomocą metody Next()
         * z klasy Random. Jeśli dla danej komórki wypadnie 0 to jest ona martwa,
         * jesli 1 to jest żywa.
         **/
        Program(int sizeOfGame1)
        {
            sizeOfGame = sizeOfGame1;
            board = new char[sizeOfGame, sizeOfGame];
            newBoard = new char[sizeOfGame, sizeOfGame];
            Random getRandom = new Random();
            for (int i = 0; i < sizeOfGame; i++)
            {
                for (int j = 0; j < sizeOfGame; j++)
                {
                    int randomNumber = getRandom.Next(0, 2);
                    if (randomNumber == 0)
                        board[i, j] = deadCell;
                    else
                        board[i, j] = aliveCell;
                }
            }   
        }

        //Metoda wyświetlająca tablicę z komórkami
        void printBoard()
        {
            for (int i = 0; i<sizeOfGame; i++)
            {
                for (int j = 0; j < sizeOfGame; j++)
                    Console.Write(this.board[i, j]);
                Console.WriteLine();
            }
        }

        /** Metoda sprawdzająca czy dana komórka jest żywa.
         * Jako argument przyjmuje współrzędne komórki, a zwraca
         * prawdę jeśli komórka jest zywa lub fałsz jeśli nie.
         **/
        public bool isAlive(int x, int y)
        {
            if (this.board[x, y] == deadCell)
                return false;
            else
                return true;
        }

        /** Metoda sprawdzająca czy dana komórka, mieści się
         * w planszy. Jako argument przyjmuje współrzędne a zwraca prawdę
         * lub fałsz.
         **/
        public bool isInBoard(int x, int y)
        {
            if (x >= 0 && x < sizeOfGame)
            {
                if (y >= 0 && y < sizeOfGame)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /** Metoda sprawdzająca czy dana komórka jest żywa
         * Argumentem są współrzędne komórki, zwraca prawde jeśli jest zywa
         * lub fałsz kiedy nie.
         **/
        public int livingNeighbour(int x, int y)
        {
            if (this.isInBoard(x, y))
            {
                if (this.isAlive(x, y))
                    return 1;
                else
                    return 0;
            }
            else
                return 0;
        }

        /** Metoda zliczająca ile żywych komórek jest wokół komórki,
         * której współrzedne zostały podane jako argumenty.
         * Zwraca wartość typu INT, która odpowiada ilości żyjących sąsiadów
         **/
        public int howManyLivingNeighbour(int x, int y)
        {
            int howMany = 0;
            howMany = this.livingNeighbour(x - 1, y - 1) + this.livingNeighbour(x, y - 1) +
                          this.livingNeighbour(x + 1, y - 1) + this.livingNeighbour(x - 1, y) +
                          this.livingNeighbour(x + 1, y) + this.livingNeighbour(x - 1, y + 1) +
                          this.livingNeighbour(x, y + 1) + this.livingNeighbour(x + 1, y + 1);

            return howMany;

        }

        /** Metoda, która określa czy dana komórka będzie w nastepnej turze
         * żywa czy martwa. Metoda zwraca 1, jeśli komórka bedzie zywa
         * lub 0 jeśli będzie martwa
         **/
        public int statusOfCell(int x, int y)
        {
            if (this.isAlive(x, y))
            {
                if (this.howManyLivingNeighbour(x, y) < 2)
                    return 0;
                else if (this.howManyLivingNeighbour(x, y) == 2 || this.howManyLivingNeighbour(x, y) == 3)
                    return 1;
                else if (this.howManyLivingNeighbour(x, y) > 3)
                    return 0;
            }
            else
            {
                if (this.howManyLivingNeighbour(x, y) < 3)
                    return 0;
                else if (this.howManyLivingNeighbour(x, y) == 3 )
                    return 1;
                else if (this.howManyLivingNeighbour(x, y) > 3)
                    return 0;
            }
            return -1;
        }

        /** Metoda, która odpowiada za określenie stanu planszy w nastepnej
         * turze. Sprawdzana jest sytuacja każdej komórki i na tej podstawie do nowej
         * planszy przypisywany jest odpowiedni stan.
         **/
        void gameOfLife()
        {
            for (int i = 0; i < sizeOfGame; i++)
            {
                for (int j = 0; j < sizeOfGame; j++)
                {
                    switch (this.statusOfCell(i, j))
                    {
                        case 0:
                            this.newBoard[i, j] = deadCell;
                            break;
                        case 1:
                            this.newBoard[i, j] = aliveCell;
                            break;
                    }
                }
            }
        }

        /** Metoda odpowiadająca za mechanikę gry. Na początku wyświetlana jest 
         * aktualna plansza, a następnie nastepuje wyliczenie układu planszy,
         * w nastepnej turze. Po naciśnięciu klawisza przez uzytkownika, konsola 
         * zostaje wyczyszczona oraz wyświetlana zostaje nowa plansza.
         **/
        void game()
        {
            while(true)
            {
                this.printBoard();
                this.gameOfLife();
                Console.ReadLine();
                this.board = this.newBoard;
                Console.Clear();   
            }   
        }
    }
}
